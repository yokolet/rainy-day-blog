require 'rails_helper'

RSpec.describe "Oauths", type: :request do
  describe "GET /twitter" do
    it "returns http success" do
      stub_request(:post, "https://api.twitter.com/2/oauth2/token").
        with(
          body: {"client_id" => "REw3QnZQeVlBek81S0VOdTBBT0Y6MTpjaQ", "code" => "code1234", "code_verifier" => nil, "grant_type" => "authorization_code", "redirect_uri" => "http://www.localhost:3000/oauth/twitter"},
          headers: {
            'Accept'=>'*/*',
            'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
            'Authorization'=>'Basic UkV3M1FuWlFlVmxCZWs4MVMwVk9kVEJCVDBZNk1UcGphUTpDTjdqSFZPTEk0aUxIMXk3N2hNUDZ6R2xTcUQtdkxrckJvQWtyd2VPZ0RJZnBKcDMwXw==',
            'Content-Type'=>'application/x-www-form-urlencoded',
            'User-Agent'=>'Faraday v2.12.2'
          }).
        to_return(status: 200, body: {access_token: 'token', expires_in: 'tomorrow'}.to_json, headers: {})

      stub_request(:get, "https://api.twitter.com/2/users/me").
        with(
          headers: {
            'Accept'=>'*/*',
            'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
            'Authorization'=>'Bearer token',
            'Content-Type'=>'application/x-www-form-urlencoded',
            'User-Agent'=>'Faraday v2.12.2'
          }).
        to_return(status: 200, body: {data: {username: 'username', name: 'who'}}.to_json, headers: {})

      get "/oauth/twitter", params: {state: 'state', code: 'code1234'}
      expect(response).to have_http_status(:redirect)
    end
  end

end
